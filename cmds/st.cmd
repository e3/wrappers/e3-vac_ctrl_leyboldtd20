require vac_ctrl_leyboldtd20

epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_leyboldtd20_DB}")
iocshLoad(${vac_ctrl_leyboldtd20_DIR}/vac_ctrl_leyboldtd20_moxa.iocsh, "DEVICENAME = vept, IPADDR = localhost, PORT = 4000")
iocshLoad(${vac_ctrl_leyboldtd20_DIR}/vac_pump_leyboldtd20_vpt.iocsh, "DEVICENAME = vpt, CONTROLLERNAME = vept")
